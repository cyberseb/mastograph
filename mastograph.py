# Mastodon social circle 2 GraphML
import sys
import os.path
from mastodon import Mastodon
from pygraphml import GraphMLParser
from pygraphml import Graph


# Please add your instance details below!
# This code is not able to deal with MFA, you might have to disable 2-Factor Auth until you have generated a user_secret.
masto_instance = 'https://ioc.exchange'
masto_user = 'seb@mail.ioc.exchange'
masto_password = 'secretpassword'
# if you want your access key stored in a safe location, change the two lines below!
client_secret = os.path.abspath(os.getcwd()) + '/mastograph_clientcred.secret'
user_secret = os.path.abspath(os.getcwd()) + '/mastograph_usercred.secret'

# Dictionary for Statistics
follower_domains = {}
mgraph = Graph()
instance_nodes = {}
user_nodes = {}


# Get domain from Follower Account
def domainsplit(x):
    try:
        return x.split('@')[1]
    except:
        return 'ioc.exchange'

# Process Follower for Statistics
def process_follower(follower):
    print(follower['acct'])
    follower_domain = domainsplit(follower['acct'])
    # Additional user of known instance?
    if follower_domain in follower_domains:
            # simple stats
            follower_domain_oldvalue = follower_domains[follower_domain]
            follower_domains[follower_domain] = follower_domain_oldvalue + 1
            # add user to social graph
            node_instance = instance_nodes[follower_domain]
            node_user = mgraph.add_node(follower['acct'])
            mgraph.add_edge(node_instance, node_user)
    # Additional user of previously unknown instance
    else:
            # simple stats
            follower_domains[follower_domain] = 1
            # add instance and user social graph
            node_instance = mgraph.add_node(follower_domain)
            node_user = mgraph.add_node(follower['acct'])
            mgraph.add_edge(node_root, node_instance)
            mgraph.add_edge(node_instance, node_user)
            # save instance and user nodes
            instance_nodes[follower_domain] = node_instance
            user_nodes[follower['acct']] = node_user
    return;


if __name__== '__main__':
    
    # Are we registered?
    if not (os.path.isfile(client_secret) and os.path.isfile(user_secret)):
        print ('Not registered with " + masto_instance + " yet. \n   Starting Registration...')
        # Register Mastodon App
        Mastodon.create_app(
            'mastograph',
            api_base_url = masto_instance,
            to_file = client_secret
        )
        # Login to generate user_secret
        mastodon = Mastodon(
            client_id = client_secret,
            api_base_url = masto_instance
        )
        mastodon.log_in(
            masto_user,
            masto_password,
            to_file = user_secret
        )
    else:
        print ('Found client secret :-) \n   Connecting...')
    
    # Login to API
    mastodon = Mastodon(
        access_token = user_secret,
        api_base_url = masto_instance
    )
    
    # Get your account ID
    account_id = mastodon.me()['id']
    print('Your account ID is ' + str(account_id) + '\n   Starting to download your followers...')
    followers_downloaded = 0
    
    # Add first node to graph
    node_root = mgraph.add_node(mastodon.me()['acct'])
    mgraph.set_root(node_root)
    
    # Get latest followers (max 80) of account
    followers_received = 0
    followers = mastodon.account_followers(id=account_id, limit=80)
    for follower in followers:
        followers_received = followers_received + 1
        process_follower(follower)
        followers_downloaded = followers_downloaded + 1
    
    # Page through all followers of account, if necessary
    while hasattr(followers[followers_received-1], '_pagination_next'):
        next_max_id = followers[followers_received-1]._pagination_next['max_id']
        # Reset counter for next API call
        followers_received = 0
        # Make next API call
        followers = mastodon.account_followers(id=account_id, max_id=next_max_id, limit=80)
        for follower in followers:
            followers_received = followers_received + 1
            process_follower(follower)
            followers_downloaded = followers_downloaded + 1
        
    
    # Print simple Stats
    print('\nDownloaded ' + str(followers_downloaded) + ' followers from the following Instances:')
    sorted_follower_domains = sorted(follower_domains.items(), key=lambda x: x[1], reverse=True)
    for domain in sorted_follower_domains:
        print(domain[0] + ': ' + str(domain[1]))
    
    # Show social graph
    mgraph.show()

    # We are done for today...
    sys.exit()