# MastoGraph

Python code that collects some simple stats from your Mastodon followers (like which instance is home to the majority of your followers) and draws a graph showing your followers based on their instance.

Before you can run this successfully, you'll need to...
1. **Install a couple of python packages**
- mastodon.py
- pygraphml
- matplotlib
- networks
2. **Fill out the connection details at the top of the file**
- masto_instance (your Mastodon instance)
- masto_user (email address you use to login to Mastodon)
- masto_password (your super secret password)
After the script has created its API keys, you can remove the password from the code.
*TODO: Ask for connection details during runtime*

Here is the output of my followers last time I ran this:

![seb's circles](sebs_circles.png)
![seb's followers](sebs_graph.png)

To allow for exploration of the graph, you could save the graph as a graphml file (instructions can be found here: https://github.com/hadim/pygraphml/blob/master/notebooks/documentation.ipynb ) and then open it in https://socnetv.org/ - Have FUN!

This code has been hacked together very quickly and might have some serious issues. Use at your own risk.
